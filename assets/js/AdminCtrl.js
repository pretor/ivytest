app.controller("AdminCtrl", function($scope, $http, $timeout) {
  $scope.name = 'admin';
  $scope.product = {
    name : '',
    price : ''
  };

  $scope.alerts = [];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.add = function(){
    $http({
      method: 'POST',
      url: '/product/add/new',
      data: $scope.product
    }).then(function successCallback(response) {
      console.log(response);
      if(response.data[0].error){
        angular.forEach(response.data, function(value, key) {
          $scope.alerts.push({type: 'danger', msg: value.message + value.field});
          $timeout(function(){
            $scope.alerts.splice(0, 1);
          },2000)
        });
      }else{
        $scope.alerts.push({type: 'success', msg: response.data[0].message});
        $timeout(function(){
          $scope.alerts.splice(0, 1);
        },2000);
        $scope.product = {
          name : '',
          price : ''
        };
      }
    }, function errorCallback(response) {
      console.log(response);
    });
  };

});

app.directive('validNumber', function() {
  return {
    require: '?ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      if(!ngModelCtrl) {
        return;
      }

      ngModelCtrl.$parsers.push(function(val) {
        if (angular.isUndefined(val)) {
          var val = '';
        }
        var clean = val.replace(/[^0-9\.]/g, '');
        var decimalCheck = clean.split('.');

        if(!angular.isUndefined(decimalCheck[1])) {
          decimalCheck[1] = decimalCheck[1].slice(0,2);
          clean =decimalCheck[0] + '.' + decimalCheck[1];
        }
        if (val !== clean) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        return clean;
      });
      element.bind('keypress', function(event) {
        if(event.keyCode === 32) {
          event.preventDefault();
        }
      });
    }
  };
});

app.directive('validAlphaNumeric', function() {
  return {
    require: '?ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      if(!ngModelCtrl) {
        return;
      }
      ngModelCtrl.$parsers.push(function(val) {
        if (angular.isUndefined(val)) {
          var val = '';
        }
        var clean = val.replace(/[^a-zA-Z0-9\s]*$/g, '');

        if (val !== clean) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        return clean;
      });
    }
  };
});