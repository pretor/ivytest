app.controller("HomeCtrl", function($scope, $http) {
  $scope.name = 'home';
  $scope.products = [];
  $scope.user = {};
  $scope.totalItems = 5;
  $scope.currentPage = 1;

  $http({
    method: 'GET',
    url: '/product/count/all'
  }).then(function successCallback(response) {
    console.log(response);
    $scope.totalItems = parseInt(response.data);
  }, function errorCallback(response) {
    console.log(response)
  });

  $http({
    method: 'POST',
    url: '/product',
    data: {limit : 5, offset : 0}
  }).then(function successCallback(response) {
    console.log(response);
    $scope.products = response.data;
  }, function errorCallback(response) {
    console.log(response)
  });
  $http({
    method: 'POST',
    url: '/user',
    data: {limit : 1, offset : 0}
  }).then(function successCallback(response) {
    console.log(response);
    $scope.user = response.data[0];
  }, function errorCallback(response) {
    console.log(response)
  });

  $scope.add = function(product_id){
    console.log(product_id)

    $http({
      method: 'POST',
      url: '/user/product/add/',
      data: { user_id: $scope.user.id, product_id: product_id }
    }).then(function successCallback(response) {
      console.log(response);
      if(response.data.message=='success'){
        angular.forEach($scope.products, function(value, key) {
          if(value.id == product_id){
            value.user = $scope.user.id;
          }
        });
      }
    }, function errorCallback(response) {
      console.log(response);
    });
  };

  $scope.remove = function(product_id){
    console.log(product_id)
    $http({
      method: 'POST',
      url: '/user/product/remove/',
      data: { user_id: $scope.user.id, product_id: product_id }
    }).then(function successCallback(response) {
      console.log(response);
      if(response.data.message=='success'){
        angular.forEach($scope.products, function(value, key) {
          if(value.id == product_id){
            value.user =null;
          }
        });
      }
    }, function errorCallback(response) {
      console.log(response);
    });
  };
  $scope.pageChanged = function() {
    var offset = $scope.currentPage == 1 ? 0 : ($scope.currentPage - 1) * 5;
    $http({
      method: 'POST',
      url: '/product',
      data: {limit : 5, offset : offset}
    }).then(function successCallback(response) {
      console.log(response);
      $scope.products = response.data;
    }, function errorCallback(response) {
      console.log(response)
    });
  };
});