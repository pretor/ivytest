<?php


namespace Ivy\Core;

/**
 * Class Validator
 * @package Ivy\Core
 */
class Validator {
  /**
   * @var bool
   */
  protected $isValid = true;
  /**
   * @var array
   */
  protected $error = array();

  /**
   * @return boolean
   */
  public function getIsValid()
  {
    return $this->isValid;
  }

  /**
   * @param boolean $isValid
   */
  public function setIsValid($isValid)
  {
    $this->isValid = $isValid;
  }

  /**
   * @return array
   */
  public function getError()
  {
    return $this->error;
  }

  /**
   * @param array $error
   */
  public function setError($error)
  {
    $this->error = $error;
  }

  /**
   * @param BaseModelInterface $model
   * @return bool
   */
  public function validate(BaseModelInterface $model){
    $validationRules = $model->getSettings()['validation_rules'];

    foreach($validationRules as $field => $rules){
      $modelPropertyMethod = 'get' . ucfirst($field);
      $value = $model->$modelPropertyMethod();
      $this->_checkValueForRules($value,$rules,$field);
    }
    return $this->isValid;
  }

  /**
   * @param $value
   * @param $rules
   * @param $field
   */
  private function _checkValueForRules($value,$rules,$field){
    foreach($rules as $rule){
      $ruleOperator = $this->_mapRuleToOperator($rule);
      $method = $ruleOperator['operator'];
      if($this->$method($value)){
        $ruleOperator['field']=$field;
        array_push($this->error,$ruleOperator );
        $this->isValid = false;
      }
    }
  }

  /**
   * @param $rule
   * @return array
   */
  private function _mapRuleToOperator($rule){
    $map = array(
        'notEmpty' =>array(
            'operator' =>  'emptyString',
            'message' =>  'Please fill required field ',
            'error'=>'empty',
        )
    );
    return $map[$rule];
  }

  /**
   * @param $str
   * @return bool
   */
  public function emptyString($str){
    return empty($str) ;
}
}