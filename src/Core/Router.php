<?php

namespace Ivy\Core;

/**
 * Class Router
 * @package Ivy\Core
 */
class Router {
  /**
   * @var array string
   */
  protected $uri;
  /**
   * @var mixed
   */
  protected $params;

  function __construct($params) {
    $uri_parts = explode('/',$params["REQUEST_URI"]);
    $this->uri = $uri_parts;
    $this->params = json_decode(file_get_contents('php://input'),true);
  }

  /**
   * Creates controller and calls method with params
   */
  public function execute(){
    $map = array(
        'home' => '\HomeController',
        'admin' => '\AdminController',
        'product' => '\ProductController',
        'user' => '\UserController'
    );

    if($this->uri[1] == ''){
      $this->uri[1] = 'home';
    }

    $name = 'Ivy\Controller'.$map[$this->uri[1]];
    $controller = new $name;
    $methodName = $this->getMethodNameFromUri();
    $controller->$methodName($this->params);
  }

  /**
   * @return string
   */
  function getMethodNameFromUri(){
    $methodName = 'index';
    if (isset($this->uri[2]) && isset($this->uri[3])) {
      $methodName = $this->uri[2] . $this->uri[3];
    }
    return $methodName;
  }
}
