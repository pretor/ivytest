<?php
namespace Ivy\Core\DBAL;
use Ivy\Core\BaseModelInterface;
use Ivy\Model\Product;
use Ivy\Model\User;

/**
 * Class BaseModel
 * @package Ivy\Core\DBAL
 */
class RDBManager implements DBManagerInterface
{

  /**
   * @var \PDO
   */
  protected $conn;

  function __construct()
  {
    try {
      $this->conn = new \PDO('mysql:host=localhost;dbname=ivy', 'root', '');
      $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    } catch (\PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }

  /**
   * @param BaseModelInterface $model
   * @return array
   */
  public function insert(BaseModelInterface $model)
  {
    try {

      $sql = $this->_buildInsertSqlQuery($model);

      $stmt = $this->conn->prepare($sql);
      $res = $stmt->execute();

      if ($res) {
        return array(
            array('message' => $model->getSettings()['message'])
        );
      }

    } catch (\PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }

  public function update()
  {

  }

  public function delete()
  {

  }
  public function select(BaseModelInterface $model)
  {

  }

  /**
   * @param BaseModelInterface $model
   * @return array
   */
  public function selectUser(BaseModelInterface $model)
  {
    try {
      $sql = "SELECT user.user_id, user.first_name ,user.last_name,  GROUP_CONCAT(up.product_id) as product_ids,  GROUP_CONCAT(p.name) as product_names,  GROUP_CONCAT(p.price) as product_prices from " . $model->getSettings()['table_name'] . " LEFT JOIN user_product up ON(user.user_id = up.user_id) LEFT JOIN product p ON(p.product_id = up.product_id)";

      $stmt = $this->conn->prepare($sql);
      $stmt->execute();

      $objectArray = array();
      foreach ($stmt->fetchAll() as $row) {

       // echo json_encode($row); die;


        $modelClass = get_class($model);
        $user = new $modelClass;
        $user->setFirstName($row['first_name']);
        $user->setLastName($row['last_name']);
        $user->setUserId($row['user_id']);
        if($row['product_ids']){
         $productIdsArray = explode(',', $row['product_ids']);
         $productNamesArray = explode(',', $row['product_names']);
         $productPricesArray = explode(',', $row['product_prices']);
          $x = 0;
          foreach($productIdsArray as $productId){
            $product = new Product();
            $product->setProductId($productId);
            $product->setName($productNamesArray[$x]);
            $product->setPrice($productPricesArray[$x]);
            $user->setProduct($product);
            $x++;
          }
        }
        array_push($objectArray, $user);
      }
      return $objectArray;

    } catch (\PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }

  /**
   * @param BaseModelInterface $model
   * @param $limit
   * @param $offset
   * @return array
   */
  public function selectProduct(BaseModelInterface $model, $limit, $offset)
  {
    try {
      $sql = "SELECT DISTINCT(product.product_id) , product.name ,product.price, product.created_at,  up.user_id as user_id, u.first_name as fname from " . $model->getSettings()['table_name'] . " LEFT JOIN user_product up ON(product.product_id = up.product_id) LEFT JOIN user u ON(u.user_id = up.user_id) LIMIT " . $offset . ", " . $limit;

      $stmt = $this->conn->prepare($sql);
      $stmt->execute();

      $objectArray = array();
      foreach ($stmt->fetchAll() as $row) {
        $modelClass = get_class($model);
        $product = new $modelClass;
        $product->setName($row['name']);
        $product->setPrice($row['price']);
        $product->setProductId($row['product_id']);
        $product->setCreatedAt($row['created_at']);
        if (isset($row['user_id'])) {
          $user = new User();
          $user->setUserId($row['user_id']);
          $product->setUser($user);
        }
        array_push($objectArray, $product);
      }
      return $objectArray;

    } catch (\PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }


  /**
   * Creates table for given model
   * @param BaseModelInterface $model
   */
  public function createTable(BaseModelInterface $model)
  {
    try {
      $stmt = $this->conn->prepare("CREATE TABLE IF NOT EXISTS " . $model->getSettings()['table_name'] . $model->getSettings()['sql'] . " CHARACTER SET utf8 COLLATE utf8_general_ci");
      $stmt->execute();
      if (isset($model->getSettings()['onetomany'])) {
        $stmt = $this->conn->prepare($model->getSettings()['onetomany']);
        $stmt->execute();
      }
    } catch (\PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }

  /**
   * @param $model
   * @param $product_id
   * @param $user_id
   * @return array
   */
  public function addProduct($model, $product_id, $user_id)
  {
    try {
      $sql = "INSERT INTO user_product(user_id, product_id) VALUES ('$user_id', '$product_id')";
      $stmt = $this->conn->prepare($sql);
      if ($stmt->execute()) {
        return array('message' => 'success');
      }
    } catch (\PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }

  /**
   * @param $model
   * @param $product_id
   * @param $user_id
   * @return array
   */
  public function removeProduct($model, $product_id, $user_id)
  {
    try {
      $sql = "DELETE FROM user_product WHERE product_id =  :product_id AND user_id = :user_id";
      $stmt = $this->conn->prepare($sql);
      if ($stmt->execute(array('product_id' => $product_id, 'user_id' => $user_id))) {
        return array('message' => 'success');
      }
    } catch (\PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }

  /**
   * Count records from table
   * @param $model
   * @return mixed
   */
  public function count($model)
  {
    try {
      $sql = "SELECT COUNT(*) as result FROM " . $model->getSettings()['table_name'];
      $stmt = $this->conn->prepare($sql);
      $stmt->execute();
      return $stmt->fetch();
    } catch (\PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }
  }

  /**
   * @param BaseModelInterface $model
   * @return string
   */
  private function _buildInsertSqlQuery(BaseModelInterface $model)
  {
    $fields = $model->getSettings()['fields'];
    $tableName = $model->getSettings()['table_name'];
    $fieldsString = $this->_arrayToString($fields);
    $values = $this->_createInsertValues($model, $fields);
    return $sql = "INSERT INTO $tableName ($fieldsString) VALUES ($values)";
  }

  /**
   * @param $fields
   * @return string
   */
  private function _arrayToString($fields)
  {
    return implode(",", $fields);
  }

  /**
   * @param $model
   * @param $fields
   * @return string
   */
  private function _createInsertValues(BaseModelInterface $model, $fields)
  {
    $values = '';

    foreach ($model->getSettings()['fields'] as $field) {
      if (strpos($field, '_') !== false) {
        $pieces = explode('_', $field);
        $fieldN = ucfirst($pieces[0]) . ucfirst($pieces[1]);
      } else {
        $fieldN = ucfirst($field);
      }
      $modelPropertyMethod = "get" . $fieldN;
      $value = $model->$modelPropertyMethod();
      $values .= "'$value',";
    }

    $values = substr($values, 0, -1);

    return $values;
  }
}