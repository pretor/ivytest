<?php
namespace Ivy\Core\DBAL;
use Ivy\Core\BaseModelInterface;
use Ivy\Model\User;

/**
 * Class BaseModel
 * @package Ivy\Core\DBAL
 */
class DBManager implements DBManagerInterface
{

  /**
   * @var \PDO
   */
  protected $driver;

  function __construct()
  {
    $this->driver = new RDBManager();
  }

  /**
   * @param BaseModelInterface $model
   * @return array
   */
  public function insert(BaseModelInterface $model)
  {
    return $this->driver->insert($model);
  }

  public function update()  {
  }

  public function delete()  {

  }
  public function select(BaseModelInterface $model){

  }

  /**
   * @param BaseModelInterface $model
   * @return array
   */
  public function selectUser(BaseModelInterface $model)  {
    return $this->driver->selectUser($model);
  }

  /**
   * @param BaseModelInterface $model
   * @param $limit
   * @param $offset
   * @return array
   */
  public function selectProduct(BaseModelInterface $model, $limit, $offset){
    return $this->driver->selectProduct($model, $limit, $offset);
  }


  /**
   * Creates table for given model
   * @param BaseModelInterface $model
   */
  public function createTable(BaseModelInterface $model){
     $this->driver->createTable($model);
  }

  /**
   * @param $model
   * @param $product_id
   * @param $user_id
   * @return array
   */
  public function addProduct(BaseModelInterface $model, $product_id, $user_id) {
    return $this->driver->addProduct($model, $product_id, $user_id);
  }

  /**
   * @param $model
   * @param $product_id
   * @param $user_id
   * @return array
   */
  public function removeProduct(BaseModelInterface $model, $product_id, $user_id){
    return $this->driver->removeProduct($model, $product_id, $user_id);
  }

  /**
   * Count records from table
   * @param $model
   * @return mixed
   */
  public function count(BaseModelInterface $model){
    return $this->driver->count($model);
  }
}