<?php

namespace Ivy\Core\DBAL;

use Ivy\Core\BaseModelInterface;

/**
 * Interface BaseModelInterface
 * @package Ivy\Core\DBAL
 */
interface DBManagerInterface{
  public function insert(BaseModelInterface $model);
  public function update();
  public function delete();
  public function select(BaseModelInterface $model);
}