<?php

namespace Ivy\Core;

/**
 * Class BaseController
 * @package Ivy\Core
 */
abstract class BaseController {
  /**
   * @param $template
   */
 public function render($template){
   echo file_get_contents(__DIR__ . "/../View/header.html");
   echo file_get_contents(__DIR__ . "/../View/".$template);
   echo file_get_contents(__DIR__ . "/../View/footer.html");
 }

  /**
   * @param $data
   */
  public function renderJson($data){
    header('Content-Type: application/json');
    echo json_encode($data);
  }

  /**
   * @param $str
   */
  public function renderString($str){
    echo $str;
  }
}