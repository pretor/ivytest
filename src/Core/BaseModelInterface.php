<?php

namespace Ivy\Core;
/**
 * Interface BaseModelInterface
 * @package Ivy\Core
 */
interface BaseModelInterface{
  function getSettings();
}