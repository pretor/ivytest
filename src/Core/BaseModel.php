<?php

namespace Ivy\Core;

use Ivy\Core\DBAL\DBManager;
use Ivy\Core\DBAL\DBManagerInterface;

/**
 * Class BaseModel
 * @package Ivy\Core
 */
abstract class BaseModel implements BaseModelInterface{
  /**
   * @var DBManagerInterface
   */
  protected $DBmanager;

  public function __construct() {
    $this->DBmanager = new DBManager();
    $this->validator = new Validator();
  }

  /**
   * @return array
   */
  public function save(){
    $this->validator->validate($this);
    if($this->validator->getIsValid()){
      return $this->DBmanager->insert($this);
    }else{
      return $this->validator->getError();
    }
  }

  public function update(){
  }

  public function delete(){
  }

  public function read($params){
    //return $this->DBmanager->select($this, $params['limit'], $params['offset']);
  }

  /**
   * @return mixed
   */
  public function count(){
    return $this->DBmanager->count($this);
  }
}