<?php

namespace Ivy\Model;

use Ivy\Core\BaseModel;

/**
 * Class Product
 * @package Ivy\Model
 */
Class Product extends BaseModel{
  /**
   * @var integer
   */
  protected $product_id;
  /**
   * @var string
   */
  protected $name;

  /**
   * @var float
   */
  protected $price;
  /**
   * @var \Datetime
   */
  protected $created_at;

  /**
   * @var User
   */
  protected $user;

  /**
   * This should be parsed from yaml/xml file as config
   * @var array
   */
  protected $settings = array(
      'fields' => array('name', 'price'),
//      'product_id' => 'integer',
//      'name' => 'string',
//      'price' => 'string',
//      'created_at' => 'datetime',
      'table_name' => 'product',
      'sql' => '(`product_id` INT AUTO_INCREMENT NOT NULL, `name` varchar(200) NOT NULL, `price` decimal(8, 2) NOT NULL, `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`product_id`))',
      'manytoone' => 'user',
      'validation_rules' => array(
        'name' => array('notEmpty'),
        'price' => array('notEmpty')
      ),
      'message' => 'You successfully added product!'
  );

  /**
   * @return int
   */
  public function getProductId()
  {
    return $this->product_id;
  }

  /**
   * @param int $product_id
   */
  public function setProductId($product_id)
  {
    $this->product_id = $product_id;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return float
   */
  public function getPrice()
  {
    return $this->price;
  }

  /**
   * @param float $price
   */
  public function setPrice($price)
  {
    $this->price = $price;
  }

  /**
   * @return \Datetime
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * @param \Datetime $created_at
   */
  public function setCreatedAt($created_at)
  {
    $this->created_at = $created_at;
  }

  /**
   * @return array
   */
  public function getSettings()
  {
    return $this->settings;
  }

  /**
   * @return User
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * @param User $user
   */
  public function setUser(User $user)
  {
    $this->user = $user;
  }

  /**
   * @param $params
   * @return array
   */
  public function toArray($params){
    $products = $this->getProducts($params);
    $productsArray = array();
    foreach($products as $product){
      array_push($productsArray, $this->objectToArray($product));
    }
    return $productsArray;
  }

  /**
   * @param $product
   * @return array
   */
  public function objectToArray($product){
    $productArray = array();
    $productArray['id'] = $product->getProductId();
    $productArray['name'] = $product->getName();
    $productArray['price'] = $product->getPrice();
    $productArray['created'] = $product->getCreatedAt();
    if($product->getUser()){
      $productArray['user'] = $product->getUser()->getUserId();
    }else{
      $productArray['user'] = null;
    }
    return $productArray;
  }

  /**
   * @param $params
   * @return array
   */
  public function getProducts($params){
    return $this->DBmanager->selectProduct($this, $params['limit'], $params['offset']);
  }

}