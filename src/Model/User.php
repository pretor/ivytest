<?php

namespace Ivy\Model;
use Ivy\Core\BaseModel;
use Ivy\Core\DBAL\DBManager;

/**
 * Class User
 * @package Ivy\Model
 */
Class User extends BaseModel{

  public function __construct() {
    parent::__construct();
    $this->products = array();
  }
  /**
   * @var integer
   */
  protected $user_id;
  /**
   * @var string
   */
  protected $first_name;
  /**
   * @var string
   */
  protected $last_name;

  /**
   * @var array
   */
  protected $products;

  /**
   * This should be parsed from yaml/xml file as config
   * @var array
   */
  protected $settings = array(
      'fields' =>array('first_name', 'last_name'),
//      'user_id' => 'integer',
//      'first_name' => 'string',
//      'last_name' => 'string',
      'table_name' => 'user',
      'sql' => '(`user_id` INT AUTO_INCREMENT NOT NULL, `first_name` varchar(200) NOT NULL, `last_name` varchar(200) NOT NULL, PRIMARY KEY (`user_id`))',
      'onetomany' => 'create table user_product(user_id INT,product_id INT,foreign key (user_id) references user(user_id), foreign key (product_id) references product(product_id));',
      'validation_rules' => array(
          'firstName' => array('notEmpty'),
          'lastName' => array('notEmpty')
      ),
      'message' => 'You successfully added user!'
  );

  /**
   * @return int
   */
  public function getUserId()
  {
    return $this->user_id;
  }

  /**
   * @param int $user_id
   */
  public function setUserId($user_id)
  {
    $this->user_id = $user_id;
  }

  /**
   * @return string
   */
  public function getFirstName()
  {
    return $this->first_name;
  }

  /**
   * @param string $first_name
   */
  public function setFirstName($first_name)
  {
    $this->first_name = $first_name;
  }

  /**
   * @return string
   */
  public function getLastName()
  {
    return $this->last_name;
  }

  /**
   * @param string $last_name
   */
  public function setLastName($last_name)
  {
    $this->last_name = $last_name;
  }

  /**
   * @return array
   */
  public function getSettings()
  {
    return $this->settings;
  }

  /**
   * @return array
   */
  public function getProducts()
  {
    return $this->products;
  }

  /**
   * @param Product $product
   */
  public function setProduct(Product $product)
  {
    array_push($this->products, $product);
  }

  /**
   * @param Product $product
   * @return array
   */
  public function addProduct(Product $product)
  {
    return $this->DBmanager->addProduct($this, $product->getProductId(), $this->getUserId());
  }

  /**
   * @param Product $product
   * @return array
   */
  public function removeProduct(Product $product){
    //find product in array and remove it
    return $this->DBmanager->removeProduct($this, $product->getProductId(), $this->getUserId());
  }

  /**
   * @param $params
   * @return string
   */
  public function toArray($params){
    $users = $this->getUsers($params);
    $usersArray = array();
    foreach($users as $user){
      array_push($usersArray, $this->objectToArray($user));
    }
    return $usersArray;
  }

  /**
   * @param $user
   * @return array
   */
  public function objectToArray(User $user){
    $userArray = array();
    $userArray['id'] = $user->getUserId();
    $userArray['fname'] = $user->getFirstName();
    $userArray['lname'] = $user->getLastName();
    if($user->getProducts()){
      $userArray['products'] = array();
      foreach($user->getProducts() as $product){
        $prod = array(
          'id'=>$product->getProductId(),
          'name'=>$product->getName(),
          'price'=>$product->getPrice(),
        );
        array_push($userArray['products'],$prod );
      }
    }else{
      $userArray['products'] = null;
    }
    return $userArray;
  }

  /**
   * @param $params
   * @return array
   */
  public function getUsers($params){
    return $this->DBmanager->selectUser($this);
  }

}