<?php

namespace Ivy\Controller;
use Ivy\Core\BaseController;

/**
 * Class HomeController
 * @package Ivy\Controller
 */
class HomeController extends BaseController {
  /**
   * Loads home page
   */
  public function index(){
    $this->render('home.html');
  }
}