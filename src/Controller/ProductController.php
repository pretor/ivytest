<?php

namespace Ivy\Controller;

use Ivy\Core\BaseController;
use Ivy\Model\Product;

/**
 * Class ProductController
 * @package Ivy\Controller
 */
class ProductController extends BaseController {

  /**
   * @param $params
   */
  public function index($params){
    try {
      $product = new Product();
      $this->renderJson($product->toArray($params));
    } catch(\Exception $e) {
      $this->renderJson($e->getMessage());
    }
  }

  /**
   * @param $params
   */
  public function addnew($params){
    try {
      $product = new Product();
      $product->setName($params['name']);
      $product->setPrice($params['price']);
      $result = $product->save();
      $this->renderJson($result);
    } catch(\Exception $e) {
      $this->renderJson($e->getMessage());
    }
  }

  public function countall(){
    try {
      $product = new Product();
      $this->renderString($product->count()['result']);
    } catch(\Exception $e) {
      $this->renderJson($e->getMessage());
    }
  }
}