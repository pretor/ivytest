<?php

namespace Ivy\Controller;
use Ivy\Core\BaseController;

/**
 * Class AdminController
 * @package Ivy\Controller
 */
class AdminController extends BaseController {
  /**
   * Loads admin page for product adding
   */
  public function productadd(){
    $this->render('admin.html');
  }
}