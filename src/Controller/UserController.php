<?php

namespace Ivy\Controller;

use Ivy\Core\BaseController;
use Ivy\Model\Product;
use Ivy\Model\User;

/**
 * Class UserController
 * @package Ivy\Controller
 */
class UserController extends BaseController{
  /**
   * @param $params
   */
  public function index($params){
    try {
      $user = new User();
      $this->renderJson($user->toArray($params));
    } catch(\Exception $e) {
      $this->renderJson($e->getMessage());
    }
  }

  /**
   * @param $params
   */
  public function productadd($params){
    try {
      $product = new Product();
      $product->setProductId($params['product_id']);
      $user = new User();
      $user->setUserId($params['user_id']);
      $this->renderJson($user->addProduct($product));
    } catch(\Exception $e) {
      $this->renderJson($e->getMessage());
    }
  }

  /**
   * @param $params
   */
  public function productremove($params){
    try {
      $product = new Product();
      $product->setProductId($params['product_id']);
      $user = new User();
      $user->setUserId($params['user_id']);
      $this->renderJson($user->removeProduct($product));
    } catch(\Exception $e) {
      $this->renderJson($e->getMessage());
    }
  }
}