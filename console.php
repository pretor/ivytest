<?php
require "vendor/autoload.php";

$user = new \Ivy\Model\User();
$producto = new \Ivy\Model\Product();

$DBmanager = new \Ivy\Core\DBAL\DBManager();
$DBmanager->createTable($producto);
$DBmanager->createTable($user);

$user->setFirstName('John');
$user->setLastName('Doe');
var_dump($user->save());

$products = array(
  array('name'=>'Watch', 'price'=>'60'),
  array('name'=>'Shoes', 'price'=>'15'),
  array('name'=>'Dog', 'price'=>'23'),
  array('name'=>'Cat', 'price'=>'19'),
  array('name'=>'Mouse', 'price'=>'46'),
  array('name'=>'Laptop', 'price'=>'78'),
  array('name'=>'Table', 'price'=>'234'),
  array('name'=>'Plate', 'price'=>'34'),
  array('name'=>'Computer', 'price'=>'67'),
  array('name'=>'Phone', 'price'=>'45'),
  array('name'=>'iPhone', 'price'=>'400'),
  array('name'=>'iPad', 'price'=>'300'),
  array('name'=>'Speakers', 'price'=>'45.8'),
  array('name'=>'House', 'price'=>'400000'),
  array('name'=>'TV', 'price'=>'400'),
  array('name'=>'Wallet', 'price'=>'5'),
  array('name'=>'Window', 'price'=>'300'),
  array('name'=>'Screen', 'price'=>'22'),
  array('name'=>'Cable', 'price'=>'6.7'),
);

foreach($products as $productSingle){
  $product = new \Ivy\Model\Product();
  $product->setName($productSingle['name']);
  $product->setPrice($productSingle['price']);
  var_dump($product->save());
}


