#!/bin/bash
#
set -x
sudo apt-get update
mkdir -p /etc/puppet/modules

sudo apt-get install -y curl

#if [ $(nodejs -v) == 'v0.10.40' ]; then
#echo "dont do the below, adding repo"
# should fix this !!!
#fi

curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs

npm install -g bower

puppet module install example42/puppi --version 2.1.7 --force
puppet module install example42/apache --version 2.1.4 --force
puppet module install puppetlabs/stdlib --version 4.1.0 --force
puppet module install puppetlabs/apt --version 1.4.0 --force
puppet module install example42/php --version 2.0.17 --force
puppet module install puppetlabs/mysql --version 2.1.0 --force
puppet module install willdurand/composer --version 0.0.6 --force
puppet module install maestrodev/wget --version 1.2.3 --force
# doesn't work
# puppet module install willdurand/nodejs --force
